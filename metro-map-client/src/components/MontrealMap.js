import { Icon  } from 'leaflet';
import { 
  MapContainer, 
  TileLayer, 
  Marker,
  Popup,
  Polyline
} from 'react-leaflet';

import 'leaflet/dist/leaflet.css';
import './Map.css';
import markerImage from '../img/marker-icon.png';

const customIcon = new Icon({
  iconUrl: markerImage,
  iconSize: [38, 38],
  iconAnchor: [22, 30]
});

const limeOptions = { color: 'lime' };

// See https://www.youtube.com/watch?v=jD6813wGdBA if you want to customize the map
// further (optional)

export default function MontrealMap() {
  const points =  [ 
    [45.446465999988021, -73.603118],
    [45.501342315993, -73.60383900042255],
    [45.520830163089066, -73.58006390089389],
  ];
  const attribution = 
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
  const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  
  // TODO this is a demo of react-leaflet. You will have to split the JSX below into
  // a couple of different components. Feel free to modify the CSS.
  return (
    <div className="ui-container">
      <div className="ui-controls">
        {/* TODO add UI controls */}
      </div>
      {/* See leaflet-container CSS class */}
      <MapContainer
        center={[45.5, -73.6]}
        zoom={12}
        zoomControl={true}
        updateWhenZooming={false}
        updateWhenIdle={true}
        preferCanvas={true}
        minZoom={10}
        maxZoom={16}
      >
        <TileLayer
          attribution={attribution}
          url={tileUrl}
        />    
        <Marker position={points[0]} icon={customIcon} >
          <Popup><p>A point</p></Popup>
        </Marker>
        <Marker position={points[1]} icon={customIcon} />
        <Marker position={points[2]} icon={customIcon} />
        <Polyline pathOptions={limeOptions} positions={points} />
      </MapContainer>
    </div>
  );
}