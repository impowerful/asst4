import './App.css';
import MontrealMap from './components/MontrealMap';

function App() {
  return (
    <div className="App">
      <MontrealMap />
    </div>
  );
}

export default App;
