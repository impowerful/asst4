Le présent ensemble de données est la propriété de la Société de transport de Montréal. Conséquemment, selon la clause d'attribution de la licence Creative Commons 4.0, la paternité des données doit être attribuée à la Société de transport de Montréal.

Originally in SHP (ShapeFile) from https://www.stm.info/fr/a-propos/developpeurs

Format shapefile pour les systèmes d’Information Géographique (SIG)

    Les lignes de bus et arrêts de bus contenus dans le GTFS sont disponibles en format shapefile de façon à faciliter l’intégration dans les SIG.  Les spécifications du shapefile sont disponibles ici- site anglais. Fichiers disponibles en MTM NAD83 Zone 8.

Dernière mise à jour 18 août 2023

Converted from SHP to GeoJSON at https://mygeodata.cloud/

    Output format: GeoJSON
    Output coordinate system: +proj=tmerc +lat_0=0 +lon_0=-73.5 +k=0.9999 +x_0=304800 +y_0=0 +datum=NAD83 +units=m +no_defs


https://www.stm.info/fr/presse/ressources-medias-0/images-et-logos