import express from 'express';
import readMetroStationData from './data-init.mjs';
const app = express();
const port = 3000;
const metroStations = await readMetroStationData();

app.use(express.static('../../metro-map-client/build'));

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.get('/random/:number', (req, res) => {
  const { number } = req.params;
  const { line } = req.query;
  let routeId;

  switch (line) {
  case 'blue':
    routeId = 5;
    break;
  case 'yellow':
    routeId = 4;
    break;
  case 'green':
    routeId = 1;
    break;
  case 'orange':
    routeId = 2;
    break;
  default:
    routeId = null;
  }

  let filteredStations = metroStations;
  if (routeId) {
    filteredStations = metroStations.filter((station) => station.lineColor === routeId);
  }

  const num = parseInt(number, 10);
  if (isNaN(num) || num < 1 || num > 10) {
    return res.status(400).json({ error: 'Invalid number parameter' });
  }
  const shuffledStations = shuffleArray(filteredStations);

  const randomStations = shuffledStations.slice(0, num);

  res.json(randomStations);
});

app.get('/details/:station', (req, res) => {
  const { station } = req.params;
  const stationDetails = metroStations.find(
    (s) => s.name.toLowerCase() === station.toLowerCase()
  );

  if (!stationDetails) {
    return res.status(404).json({ error: 'Station not found' });
  }

  res.json(stationDetails);
});

app.get('/random-questions/:number', (req, res) => {
  const { number } = req.params;
  const questions = [];

  for (let i = 0; i < number; i++) {
    const randomStationIndex = Math.floor(Math.random() * metroStations.length);
    const randomStation = metroStations[randomStationIndex];

    const options = [randomStation.name];

    while (options.length < 3) {
      const randomOptionIndex = Math.floor(Math.random() * metroStations.length);
      const randomOption = metroStations[randomOptionIndex].name;

      if (!options.includes(randomOption)) {
        options.push(randomOption);
      }
    }

    const correctAnswer = randomStation.name;

    const question = {
      coordinates: randomStation.coordinates,
      options,
      correctAnswer: correctAnswer,
    };

    questions.push({ question });
  }

  res.json(questions);
});

const server = app.listen(port, () => {
  console.log(`Example app app listening at http://localhost:${port}`);
});

process.on('SIGTERM', () => {
  debug('SIGTERM signal received: closing HTTP server');
  server.close(() => {
    debug('HTTP server closed');
  });
});

function shuffleArray(array) {
  const shuffledArray = [...array];
  for (let i = shuffledArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }
  return shuffledArray;
}