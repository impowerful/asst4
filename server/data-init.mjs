import fs from 'fs/promises';

const dataFilePath = './data/stm_arrets_sig_wgs84.geojson';

async function readMetroStationData() {
  try {
    const data = await fs.readFile(dataFilePath, 'utf8');
    const metroStations = JSON.parse(await data);

    // Create a Set to store unique stop_code values
    const uniqueStopCodes = new Set();

    const stationData = metroStations.features.
      filter((station) =>
        ['1', '2', '4', '5', '1,2'].includes(station.properties.route_id)
      ).
      map((station) => {
        const stopCode = station.properties.stop_code;
        if (!uniqueStopCodes.has(stopCode)) {
          uniqueStopCodes.add(stopCode);
          return {
            name: station.properties.stop_name,
            coordinates: station.geometry.coordinates,
            lineColor: station.properties.route_id,
            url: station.properties.stop_url,
          };
        }
        return null;
      }).
      filter((station) => station !== null);

    return stationData;
  } catch (error) {
    console.error('Error reading or parsing metro station data:', error);
    throw error;
  }
}


export default readMetroStationData;
